ESP MQTT Doorbell
=================

This is an implementation of a doorbell using MQTT as broker to publish information on.
The suite implements a factory reset, which enables a WiFi access point to allow the
user to configure the settings of the device. If successful, the doorbell will
restart itself as a station, with the doorbell attached and publishes an event when
the doorbell has been pressed.
