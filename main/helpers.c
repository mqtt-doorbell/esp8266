/* Copyright  2020  Sjoerd van Leent

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.  */

#include "doorbell.h"

void log_doorbell_cfg (doorbell_cfg_t* doorbell_cfg)
{
  ESP_LOGI (TAG,
	    "SSID cnt: %d, URI cnt: %d, CH cnt: %d, SSID: %s, URI %s, CH: %s",
	    doorbell_cfg->ssid_len,
	    doorbell_cfg->uri_len,
	    doorbell_cfg->channel_len,
	    doorbell_cfg->ssid,
	    doorbell_cfg->uri,
	    doorbell_cfg->channel);
}
