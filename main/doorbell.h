/* Copyright  2020  Sjoerd van Leent

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.  */

/*! \file
This is the main header file used by the ESP MQTT Doorbell project, it contains
all the necessary includes from the outside world, as well as internal includes. */

#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/stat.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <freertos/event_groups.h>
#include <esp_spiffs.h>
#include <esp_log.h>
#include <esp_wifi.h>
#include <esp_wifi_types.h>
#include <esp_event_loop.h>
#include <esp_http_server.h>
#include <esp_timer.h>
#include <tcpip_adapter.h>
#include <nvs_flash.h>
#include <mqtt_client.h>
#include <gpio.h>

#include "main/setup_htm.h"
#include "main/bootstrap_css.h"
#include "main/vue_js.h"
#include "main/jquery_js.h"

#define TAG "main"
#define USER_CNF "/spiffs/userconf"
#define MAX_APS 32


/*! This struct contains the data of the access point, password, mqtt uri and 
   channel to use. The configuration during set up is mapped in from the browser
   response, the configuration during regular runmode is read from the ROM */
typedef struct {
  uint8_t ssid_len;      /*!< The actual length of the SSID in typed characters */
  uint8_t pwd_len;       /*!< The actual length of the password */
  uint8_t uri_len;       /*!< The actual length of the MQTT URI */
  uint8_t channel_len;   /*!< The actual length of the MQTT Topic (channel) */
  char ssid[32];         /*!< The SSID as selected by the user.  */
  char pwd[64];          /*!< The password as entered by the user.  */
  char uri[256];         /*!< The MQTT URI as enttered by the user.  */
  char channel[256];     /*!< The MQTT TOPIC as enttered by the user.  */
  uint8_t reserved[412]; /*!< Alignment at 1K boundary, also for future use. */
} doorbell_cfg_t;


extern const wifi_country_t country;

/* Launch the set-up process.  */
void start_setup();


/* Start everything in regular mode.  */
void start ();

/* Log the contents of the doorbell configuration.  */
void log_doorbell_cfg (doorbell_cfg_t* doorbell_cfg);
