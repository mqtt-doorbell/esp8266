/* Copyright  2020  Sjoerd van Leent

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.  */

#include "doorbell.h"


/* This function is used to check on the availability of the user config.  */
bool has_user_conf();



/************************************************************\
 * MAIN ENTRYPOINT                                          *
\************************************************************/

void app_main()
{
  esp_timer_init ();

  ESP_ERROR_CHECK (nvs_flash_erase());
  ESP_ERROR_CHECK (nvs_flash_init());
  
  ESP_LOGI (TAG, "Attempting to find config @ /spiffs/userconf");

  static const esp_vfs_spiffs_conf_t conf =
  {
   .base_path = "/spiffs",
   .partition_label = NULL,
   .max_files = 5,
   .format_if_mount_failed = true
  };

  /* Initialize the configuration of the SPIFFS partition.  */
  ESP_ERROR_CHECK (esp_vfs_spiffs_register (&conf));
  size_t total = 0, used = 0;
  esp_spiffs_info(NULL, &total, &used);
  ESP_LOGI (TAG, "Partition size: total %d, used %d", total, used);

  if (has_user_conf())
    {
      ESP_LOGI (TAG, "Launching default startup process");
      start ();
    }
  else
    {
      ESP_LOGI (TAG, "Launching setup process");
      start_setup ();
    }
}


bool has_user_conf ()
{
  struct stat st;
  return (stat (USER_CNF, &st) == 0);
}
