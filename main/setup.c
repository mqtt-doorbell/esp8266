/* Copyright  2020  Sjoerd van Leent

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.  */

#include "doorbell.h"

/* This is the SSID actually used for set up purposes. If running, this SSID
   is still available, and will return a simple result whether the doorbell
   is running.  */
char status_ssid[32] = {0};

/* Set to verify that the connection is working.  */
uint8_t connect_ok = 0;

/* Set to verify that connection to MQTT is working.  */
uint8_t mqtt_ok = 0;

/* Amount of seconds to wait for connection.  */
static const uint8_t SETUP_WAIT_CONNECT = 10;

/* Amount of millis waiting for connection.  */

typedef enum
  {
   VALIDATE_IDLE,
   VALIDATE_START,
   VALIDATE_WIFI_CONNECT,
   VALIDATE_MQTT_CONNECT
  } validate_t;

struct
{
  int64_t time_snapshot;
  esp_timer_handle_t validate_timer_handle;
  validate_t mode;
} setup_validation;

/* This struct contains the scanned access points while in Setup mode.  */
struct {
  wifi_ap_record_t aps[MAX_APS];
  uint16_t aps_no;
} aps_scan_result;

/* This struct contains the data of the access point, password, mqtt uri and 
   channel to use.  */
static doorbell_cfg_t doorbell_cfg;

/* Arguments to create a timer.  */
static esp_timer_create_args_t valid_cfg_timer_args;

/* Launch the setup process through an access point.  */
static void start_setup_wifi_ap ();

/* Returns the auth mode name.  */
const char* auth_mode_kw (wifi_auth_mode_t mode);


/* The event handler of the setup process.  */
static esp_err_t setup_event_handler (void *ctx, system_event_t *event);

/* Initializes the status SSID.  */
static void init_status_ssid ();

/* Validates the configuration of the doorbell.  */
static void setup_validate_doorbell_cfg (void *arg);

/* Awaits for positive WiFi connection while seting up.  */
static void setup_check_wifi ();

/* Awaits for positive MQTT broker connection while seting up.  */
static void setup_check_mqtt ();

/* Checks if connection to either WiFi or MQTT took to long in the setup.  */
static void setup_timeout_check ();

/* The event handler handles MQTT connections while setting up.  */
static esp_err_t mqtt_setup_event_handler (esp_mqtt_event_handle_t event);


static void start_setup_wifi_ap ()
{
  wifi_config_t cfg =
    {
     .ap =
     {
      .ssid_len = strlen (status_ssid),
      .max_connection = 1,
      .authmode = WIFI_AUTH_OPEN
     },
    };
  memcpy(cfg.ap.ssid, status_ssid, 32);
  ESP_ERROR_CHECK (esp_wifi_set_mode (WIFI_MODE_AP));
  ESP_ERROR_CHECK (esp_wifi_set_config (ESP_IF_WIFI_AP, &cfg));
  ESP_ERROR_CHECK (esp_wifi_start ());

  ESP_LOGI (TAG, "Started as Accesspoint");
}

const char* auth_mode_kw (wifi_auth_mode_t mode)
{
  switch (mode)
    {
    case WIFI_AUTH_OPEN: return "open";
    case WIFI_AUTH_WEP: return "wep";
    case WIFI_AUTH_WPA_PSK: return "wpa";
    case WIFI_AUTH_WPA2_PSK: return "wpa2";
    case WIFI_AUTH_WPA_WPA2_PSK: return "wpa-wpa2";
    case WIFI_AUTH_WPA2_ENTERPRISE: return "wpa2-enterprise";
    default: return "unknown";
    }
}

void start_setup ()
{
  init_status_ssid ();
  ESP_ERROR_CHECK (esp_event_loop_create_default());
  tcpip_adapter_init ();
  esp_timer_init ();
  setup_validation.mode = VALIDATE_IDLE;
  valid_cfg_timer_args.callback = &setup_validate_doorbell_cfg;
  valid_cfg_timer_args.name = "validate_doorbell_cfg";

  ESP_ERROR_CHECK (esp_timer_create (&valid_cfg_timer_args, &(setup_validation.validate_timer_handle)));
  ESP_ERROR_CHECK (esp_timer_start_once (setup_validation.validate_timer_handle, 100000)); 
  
  ESP_ERROR_CHECK (esp_event_loop_init (setup_event_handler, NULL));
  wifi_init_config_t icfg = WIFI_INIT_CONFIG_DEFAULT();
  ESP_ERROR_CHECK (esp_wifi_init (&icfg));


  wifi_scan_config_t scan_cfg = {}; /* Use default configuration.  */
  ESP_ERROR_CHECK (esp_wifi_set_mode (WIFI_MODE_STA));
  esp_wifi_set_country (&country);
  ESP_ERROR_CHECK (esp_wifi_start ());
  ESP_LOGI (TAG, "Scanning for available access points");
  esp_wifi_scan_start (&scan_cfg, true);
  ESP_LOGI (TAG, "Finished scanning for available access points");
  esp_wifi_scan_get_ap_num (&(aps_scan_result.aps_no));
  ESP_LOGI (TAG, "Found %d Access Points", aps_scan_result.aps_no);
  uint16_t ap_num = MAX_APS;
  esp_wifi_scan_get_ap_records (&ap_num, aps_scan_result.aps);
  for (int i = 0; i < aps_scan_result.aps_no; i++)
    {
      ESP_LOGI (TAG, "SSID: %s, auth %s", aps_scan_result.aps[i].ssid,
		auth_mode_kw (aps_scan_result.aps[i].authmode));
    }
  ESP_ERROR_CHECK(esp_wifi_stop ());
  start_setup_wifi_ap ();

}


static esp_err_t setup_http_get_handler (httpd_req_t *req)
{
  const char* resp = SETUP_HTM;
  ESP_LOGI (TAG, "Serving setup html");
  httpd_resp_send (req, resp, strlen (resp));
  return ESP_OK;
}

static esp_err_t setup_http_bootstrap_css_handler (httpd_req_t *req)
{
  const char* resp = BOOTSTRAP_CSS;
  ESP_LOGI (TAG, "Serving bootstrap css");
  httpd_resp_set_type (req, "text/css");
  httpd_resp_send (req, resp, strlen (resp));
  return ESP_OK;
}

static esp_err_t setup_http_jquery_js_handler (httpd_req_t *req)
{
  const char* resp = JQUERY_JS;
  httpd_resp_set_type (req, "text/javascript");
  httpd_resp_send (req, resp, strlen (resp));
  return ESP_OK;
}

static esp_err_t setup_http_vue_js_handler (httpd_req_t *req)
{
  const char* resp = VUE_JS;
  httpd_resp_set_type (req, "text/javascript");
  httpd_resp_send (req, resp, strlen (resp));
  return ESP_OK;
}

static esp_err_t setup_http_aps_handler (httpd_req_t *req)
{
  httpd_resp_set_type (req, "text/plain");
  for (int i = 0; i < aps_scan_result.aps_no; i++)
    {
      /* Order is: authmode number, authmode as keyword (no spaces), SSID */
      const char *authmode = auth_mode_kw (aps_scan_result.aps[i].authmode);
      const char *ssid = (char *)aps_scan_result.aps[i].ssid;
      char authmode_n[8] = { 0, 0, 0, 0, 0, 0, 0, 0 };
      sprintf (authmode_n, "%d", aps_scan_result.aps[i].authmode);
      httpd_resp_send_chunk (req, authmode_n, strlen(authmode_n));
      httpd_resp_send_chunk (req, " " , 1);
      httpd_resp_send_chunk (req, authmode, strlen (authmode));
      httpd_resp_send_chunk (req, " " , 1);
      httpd_resp_send_chunk (req, ssid, strlen(ssid));
      httpd_resp_send_chunk (req, "\n" , 1);
    }
  httpd_resp_send_chunk (req, NULL , 0);
  return ESP_OK;
}

static esp_err_t setup_http_init_handler (httpd_req_t *req)
{
  /* Receive no more than 1024 bytes, that would make no sense.  */
  char content [1024] = {0};

  ESP_LOGI (TAG, "Content length: %d", req->content_len);
  
  if (req->content_len != 1024)
    {
      /* Content length is too long.  */
      httpd_resp_send_500 (req);
      return ESP_FAIL;
    }
  ESP_LOGI (TAG, "Content == 1024 bytes");
  int ret = httpd_req_recv(req, content, 1024);
  /* Check if data is received properly.  */
  if (ret <= 0)
    {
      if (ret == HTTPD_SOCK_ERR_TIMEOUT)
	{
	  httpd_resp_send_408 (req);
	}
      return ESP_FAIL;
    }

  memcpy(&doorbell_cfg, content, sizeof (doorbell_cfg));

  log_doorbell_cfg (&doorbell_cfg);

  /* Mild input verification.  */
  if (!(doorbell_cfg.ssid_len <= 32 &&
	doorbell_cfg.pwd_len <= 64))
    {
      httpd_resp_send_500 (req);
      return ESP_FAIL;
    }

  static const char *result = "hardware_validation";
  /* If data is received, parse it.  */
  httpd_resp_send (req, result, strlen ("result"));

  /* Set validation to commence.  */
  setup_validation.mode = VALIDATE_START;
  
  /* This is never reached.  */
  return ESP_OK;
}

static void setup_validate_doorbell_cfg (void *arg)
{
  switch (setup_validation.mode)
    {
    case VALIDATE_IDLE:
      break;
    case VALIDATE_START:
      {
	/* From here on, a reboot will always occur.  */
	ESP_LOGI (TAG, "Start validation of doorbell configuration.");

	ESP_ERROR_CHECK (esp_wifi_stop ());
	ESP_ERROR_CHECK (esp_wifi_deinit ());

	wifi_config_t cfg = {};
	memcpy (cfg.sta.ssid, doorbell_cfg.ssid, 32);
	memcpy (cfg.sta.password, doorbell_cfg.pwd, 64);

	wifi_init_config_t icfg = WIFI_INIT_CONFIG_DEFAULT();
	ESP_ERROR_CHECK (esp_wifi_init (&icfg));
	ESP_ERROR_CHECK (esp_wifi_set_mode (WIFI_MODE_STA));
	ESP_ERROR_CHECK (esp_wifi_set_config (ESP_IF_WIFI_STA, &cfg));
	ESP_ERROR_CHECK (esp_wifi_start ());
  
	setup_validation.time_snapshot = esp_timer_get_time();
	setup_validation.mode = VALIDATE_WIFI_CONNECT;
      }
      break;
    case VALIDATE_WIFI_CONNECT:
      setup_check_wifi ();
      break;
    case VALIDATE_MQTT_CONNECT:
      setup_check_mqtt ();
      break;
    default:
      break;
    }
  esp_timer_delete (setup_validation.validate_timer_handle);
  ESP_ERROR_CHECK (esp_timer_create (&valid_cfg_timer_args, &(setup_validation.validate_timer_handle)));
  ESP_ERROR_CHECK (esp_timer_start_once (setup_validation.validate_timer_handle, 100000)); 
}

/* Checks if connection is successful, for SETUP_WAIT_CONNECT seconds. */
static void setup_check_wifi ()
{
  if (connect_ok)
    {
      ESP_LOGI (TAG, "Connection to WiFi network successful.");
      /* Attempt to connect to the MQTT uri.  */
      const esp_mqtt_client_config_t mqtt_cfg =
	{
	 .uri = doorbell_cfg.uri,
	 .event_handle = &mqtt_setup_event_handler
	};
      
      esp_mqtt_client_handle_t client = esp_mqtt_client_init (&mqtt_cfg);
      esp_mqtt_client_start (client);

      setup_validation.time_snapshot = esp_timer_get_time();
      setup_validation.mode = VALIDATE_MQTT_CONNECT;
    }
  else
    {
      setup_timeout_check ();
    }
  
}

static void setup_check_mqtt ()
{
  if (mqtt_ok)
    {
      /* Save the configuration to storage, everything seems to be OK. */
      ESP_LOGI (TAG, "Connection to MQTT broker successful.");
      ESP_LOGI (TAG, "Saving configuration to file.");
      FILE *file = fopen(USER_CNF, "w");
      if (file != NULL)
	{
	  fwrite (&doorbell_cfg, sizeof (doorbell_cfg), 1, file);
	  fclose (file);
	  ESP_LOGI (TAG, "Saved configuration to file, restarting.");
	}
      else
	{
	  ESP_LOGE (TAG, "Could not open configuration file @ %s", USER_CNF);
	}
      esp_restart ();
    }
  else
    {
      setup_timeout_check ();
    }
}

static esp_err_t mqtt_setup_event_handler (esp_mqtt_event_handle_t event)
{
  switch (event->event_id) {
  case MQTT_EVENT_CONNECTED:
    mqtt_ok = 1;
  default:
    break;
  }

  return ESP_OK;
}

httpd_uri_t uri_get_index =
  {
   .uri = "/",
   .method = HTTP_GET,
   .handler = setup_http_get_handler,
   .user_ctx = NULL
  };

httpd_uri_t uri_bootstrap_css =
  {
   .uri = "/bootstrap.min.css",
   .method = HTTP_GET,
   .handler = setup_http_bootstrap_css_handler,
   .user_ctx = NULL
  };

httpd_uri_t uri_jquery_js =
  {
   .uri = "/jquery.min.js",
   .method = HTTP_GET,
   .handler = setup_http_jquery_js_handler,
   .user_ctx = NULL
  };

httpd_uri_t uri_vue_js =
  {
   .uri = "/vue.js",
   .method = HTTP_GET,
   .handler = setup_http_vue_js_handler,
   .user_ctx = NULL
  };

httpd_uri_t uri_get_aps =
  {
   .uri = "/aps",
   .method = HTTP_GET,
   .handler = setup_http_aps_handler,
   .user_ctx = NULL
  };

httpd_uri_t uri_post_init =
  {
   .uri = "/init",
   .method = HTTP_POST,
   .handler = setup_http_init_handler,
   .user_ctx = NULL
  };

static esp_err_t setup_event_handler(void *ctx, system_event_t *event)
{
  switch (event->event_id)
    {
    case SYSTEM_EVENT_STA_START:
      if (setup_validation.mode == VALIDATE_START)
	{
	  ESP_LOGI (TAG, "Started WiFi Station Mode");
	  ESP_ERROR_CHECK (esp_wifi_connect ());
	}
      break;
    case SYSTEM_EVENT_STA_GOT_IP:
      connect_ok = 1;
      break;
    case SYSTEM_EVENT_STA_CONNECTED:
      ESP_LOGI (TAG, "Connected");
      break;
    case SYSTEM_EVENT_STA_DISCONNECTED:
      ESP_LOGI (TAG, "Could not connect");
      break;

    case SYSTEM_EVENT_AP_START:
      /* Start the HTTP server */
      {
	httpd_config_t config = HTTPD_DEFAULT_CONFIG();
	httpd_handle_t server = NULL;
	ESP_ERROR_CHECK (httpd_start (&server, &config));
	httpd_register_uri_handler (server, &uri_get_index);
	httpd_register_uri_handler (server, &uri_bootstrap_css);
	httpd_register_uri_handler (server, &uri_jquery_js);
	httpd_register_uri_handler (server, &uri_vue_js);
	httpd_register_uri_handler (server, &uri_get_aps);
	httpd_register_uri_handler (server, &uri_post_init);
	ESP_LOGI (TAG,
		  "Started webserver, SSID %s base address @ http://192.168.4.1/",
		  status_ssid);
      }
      break;
    case SYSTEM_EVENT_AP_STACONNECTED:
      ESP_LOGI (TAG, "station:"MACSTR" join, AID=%d",
		MAC2STR(event->event_info.sta_connected.mac),
		event->event_info.sta_connected.aid);
      break;
    case SYSTEM_EVENT_AP_STADISCONNECTED:
      ESP_LOGI (TAG, "station:"MACSTR"leave, AID=%d",
		MAC2STR(event->event_info.sta_disconnected.mac),
		event->event_info.sta_disconnected.aid);
      break;
    default:
      break;
    }
  return ESP_OK;
}

static void init_status_ssid ()
{
  uint8_t m[6];
  esp_efuse_mac_get_default (m);
  sprintf (status_ssid,
	   "EMDOORBELL-%x-%x-%x-%x-%x-%x",
	   m[0], m[1], m[2], m[3], m[4], m[5]);

}

static void setup_timeout_check ()
{
  int64_t u_current_time = esp_timer_get_time();
  int64_t micros_waiting = u_current_time - setup_validation.time_snapshot;
  int64_t s_waiting = micros_waiting / 1000000;
  if (((int64_t)SETUP_WAIT_CONNECT) < s_waiting)
    {
      ESP_LOGI (TAG, "Not connected in time, restarting.");
      esp_restart ();
    }
}
