/* Copyright  2020  Sjoerd van Leent

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.  */

#include "doorbell.h"


/* This struct contains the data of the access point, password, mqtt uri and 
   channel to use.  */
static doorbell_cfg_t doorbell_cfg;
static esp_mqtt_client_handle_t mqtt_client = NULL;
static xQueueHandle factory_reset_evt_queue = NULL;
static xQueueHandle bell_evt_queue = NULL;
static int64_t bell_time_snapshot = 0LL;


/* Regular mode WiFi event handler.  */
static esp_err_t event_handler (void *ctx, system_event_t *event);

/* Regular mode MQTT event handler.  */
static esp_err_t mqtt_event_handler (esp_mqtt_event_handle_t event);

/* Handles the interrupt service request from either button 5 or 6.  */
static void factory_reset_isr_handler (void* arg);
static void factory_reset_task (void *arg);
static void bell_isr_handler (void* arg);
static void bell_task (void *arg);


void start ()
{
  bell_time_snapshot = esp_timer_get_time();
  ESP_ERROR_CHECK (esp_event_loop_create_default());
  tcpip_adapter_init ();

  ESP_ERROR_CHECK (esp_event_loop_init (event_handler, NULL));

  FILE *file = fopen(USER_CNF, "r");
  if (file != NULL)
    {
      fread (&doorbell_cfg, sizeof (doorbell_cfg), 1, file);
      fclose (file);
    }
  log_doorbell_cfg (&doorbell_cfg);


  const esp_mqtt_client_config_t mqtt_cfg =
    {
     .uri = doorbell_cfg.uri,
     .event_handle = &mqtt_event_handler
    };
  mqtt_client = esp_mqtt_client_init (&mqtt_cfg);

  gpio_config_t gpio_cfg =
    {
     .intr_type = GPIO_INTR_NEGEDGE,
     .mode = GPIO_MODE_INPUT,
     .pin_bit_mask = (1ULL << 5),
     .pull_up_en = GPIO_PULLUP_DISABLE,
     .pull_down_en = GPIO_PULLDOWN_ENABLE,
    };
  gpio_config (&gpio_cfg);

  gpio_config_t gpio_fr_cfg =
    {
     .intr_type = GPIO_INTR_NEGEDGE,
     .mode = GPIO_MODE_INPUT,
     .pin_bit_mask = (1ULL << 4),
     .pull_up_en = GPIO_PULLUP_DISABLE,
     .pull_down_en = GPIO_PULLDOWN_ENABLE,
    };
  gpio_config (&gpio_fr_cfg);

  factory_reset_evt_queue = xQueueCreate(1, sizeof(uint32_t));
  bell_evt_queue = xQueueCreate(1, sizeof(uint32_t));

  xTaskCreate(bell_task, "bell_task", 2048, NULL, 10, NULL);
  xTaskCreate(factory_reset_task, "factory_reset_task", 2048, NULL, 10, NULL);

  gpio_install_isr_service (0);
  gpio_isr_handler_add (GPIO_NUM_5, bell_isr_handler, (void*) GPIO_NUM_5);
  gpio_isr_handler_add (GPIO_NUM_4, factory_reset_isr_handler, (void*) GPIO_NUM_4);


  ESP_LOGI (TAG, "Initialize WiFi, and connect");
  wifi_config_t cfg = {};
  memcpy (cfg.sta.ssid, doorbell_cfg.ssid, 32);
  memcpy (cfg.sta.password, doorbell_cfg.pwd, 64);

  wifi_init_config_t icfg = WIFI_INIT_CONFIG_DEFAULT();
  ESP_ERROR_CHECK (esp_wifi_init (&icfg));
  ESP_ERROR_CHECK (esp_wifi_set_mode (WIFI_MODE_STA));
  ESP_ERROR_CHECK (esp_wifi_set_config (ESP_IF_WIFI_STA, &cfg));
  ESP_ERROR_CHECK (esp_wifi_start ());
}

static void factory_reset_isr_handler (void* arg)
{
  uint32_t data = 0;
  xQueueSendFromISR (factory_reset_evt_queue, &data, NULL);
}

static void factory_reset_task (void *arg)
{
  for(;;)
    {
      uint32_t data;
      if (xQueueReceive (factory_reset_evt_queue, &data, portMAX_DELAY))
	{
	  ESP_LOGW (TAG, "!!! Factory reset !!!");
	  remove (USER_CNF);
	  esp_restart ();
	}
    }
}

static void bell_isr_handler (void* arg)
{
  uint32_t data = 0;
  xQueueSendFromISR (bell_evt_queue, &data, NULL);
}

static void bell_task (void *arg)
{
  for(;;)
    {
      uint32_t data;
      if (xQueueReceive (bell_evt_queue, &data, portMAX_DELAY))
	{
 	  int level = gpio_get_level(GPIO_NUM_5);
	  if (level == 0)
	    {
	      ESP_LOGI (TAG, "Doorbell pressed: level %d", level);
	      char *channel = doorbell_cfg.channel;
	      if (strlen (channel) > 1)
		{
		  if (channel[0] == '/')
		    {
		      channel++;
		    }
		  ESP_LOGI (TAG, "Publishing to channel %s", channel);
		  esp_mqtt_client_publish (mqtt_client, channel, "ring", 0, 1, 0);
		}
	    }
	}
    }
}

static esp_err_t event_handler(void *ctx, system_event_t *event)
{
  switch (event->event_id)
    {
    case SYSTEM_EVENT_STA_START:
      ESP_LOGI (TAG, "Started WiFi Station Mode");
      esp_wifi_connect ();
    case SYSTEM_EVENT_STA_GOT_IP:
      ESP_LOGI (TAG, "Received IP");
      {
	esp_mqtt_client_start (mqtt_client);
      }
      break;
    case SYSTEM_EVENT_STA_CONNECTED:
      ESP_LOGI (TAG, "Connected");
      break;
    case SYSTEM_EVENT_STA_DISCONNECTED:
      ESP_LOGI (TAG, "Disconnected");
      break;
    case SYSTEM_EVENT_STA_LOST_IP:
      
    default:
      break;
    }
  return ESP_OK;
}

static esp_err_t mqtt_event_handler (esp_mqtt_event_handle_t event)
{
  switch (event->event_id) {
  case MQTT_EVENT_CONNECTED:
    ESP_LOGI (TAG, "Connected to MQTT Broker");
    break;
  case MQTT_EVENT_PUBLISHED:
    ESP_LOGI (TAG, "Published message on MQTT Broker, msg_id=%d", event->msg_id);
    break;
  case MQTT_EVENT_ERROR:
    ESP_LOGI (TAG, "MQTT_EVENT_ERROR");
    break;
  default:
    ESP_LOGI (TAG, "Other event id:%d", event->event_id);
    break;
  }

  return ESP_OK;
}

